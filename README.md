## Coding Exercise 

### Requirements 

- Java 8
- Maven 3.6.3 for building JAR file.

## Notes 

The implementation permits to extend `AbstractScoreBoard` class to different scoreboards of objects implementing `IMatch` interface. 