package com.sportradar.scoreboard;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.sportradar.match.IMatch;

public abstract class AbstractScoreBoard<T extends IMatch> {

    List<T> matches = new ArrayList<>();

    /**
     * 
     * Start a game given a match.
     * 
     * @param match
     */
    public void startGame(T match) {
        matches.add(match);
    }

    /**
     * 
     * Finish a game given a match.
     * 
     * @param match a match
     */
    public void finishGame(T match) {
        matches.remove(match);
    }

    /**
     * 
     * Update game score given a match and home and away team score.
     * 
     * @param match a match
     * @param homeTeamScore home team score
     * @param awayTeamScore away team score
     */
    public void updateScore(T match, int homeTeamScore, int awayTeamScore) {
        matches.get(matches.indexOf(match)).updateScore(homeTeamScore, awayTeamScore);
    }
    
    /**
     * 
     * Get games summary sorted by scores and added order.
     * 
     * @return summary string
     */
    public String getSummary() {
        return matches.stream()
                    .sorted((o1, o2) -> matchesComparison(o1, o2))
                    .map(IMatch::toString)
                    .collect(Collectors.joining("\n"));
    }

    private int matchesComparison(T match1, T match2) {
        if (match1.getTotalScore() != match2.getTotalScore()) {
            return match2.getTotalScore() - match1.getTotalScore();
        }
        return matches.indexOf(match1) - matches.indexOf(match2);
    }

}
