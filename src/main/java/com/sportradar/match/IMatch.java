package com.sportradar.match;

public interface IMatch {

    /**
     * 
     * Update match score given home and away team scores.
     * 
     * @param homeTeamScore home team score
     * @param awayTeamScore away team score
     */
    void updateScore(int homeTeamScore, int awayTeamScore);

    /**
     * Get the sum of home and away team scores.
     * @return total of the sum
     */
    int getTotalScore();
    
}
