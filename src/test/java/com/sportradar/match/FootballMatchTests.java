package com.sportradar.match;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

public class FootballMatchTests {

    private static final FootballMatch GIVEN_FOOTBALL_MATCH = new FootballMatch("TEAM A", "TEAM B", 1, 2, LocalDateTime.now());
    
    @Test
    void getTotalScore_shouldReturnValidSum() {
        assertEquals(3, GIVEN_FOOTBALL_MATCH.getTotalScore());
    }

    @Test
    void updateScore_shouldUpdateCorrectly() {
        FootballMatch givenFootballMatch = new FootballMatch("TEAM A", "TEAM B", 1, 2, LocalDateTime.now());
        givenFootballMatch.updateScore(2, 2);
        assertEquals(4, givenFootballMatch.getTotalScore());
    }
    
    @Test
    void toString_shouldReturnValidFormat() {
        assertEquals("TEAM A 1 - TEAM B 2", GIVEN_FOOTBALL_MATCH.toString());
    }

}
