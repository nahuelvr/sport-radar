package com.sportradar.scoreboard;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import com.sportradar.match.FootballMatch;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class WorldCupScoreBoardTests {

    private static final FootballMatch MATCH_1 = new FootballMatch("TEAM A", "TEAM B", 0, 0, LocalDateTime.now());
    private static final String STR_MATCH_1 = "TEAM A 0 - TEAM B 0";
    private static final FootballMatch MATCH_2 = new FootballMatch("TEAM C", "TEAM D", 1, 2, LocalDateTime.now());
    private static final String STR_MATCH_2 = "TEAM C 1 - TEAM D 2";
    private static final FootballMatch MATCH_3 = new FootballMatch("TEAM E", "TEAM F", 0, 0, LocalDateTime.now());
    private static final String STR_MATCH_3 = "TEAM E 0 - TEAM F 0";
    private static final FootballMatch MATCH_4 = new FootballMatch("TEAM G", "TEAM H", 2, 2, LocalDateTime.now());
    private static final String STR_MATCH_4 = "TEAM G 2 - TEAM H 2";
    
    WorldCupScoreBoard scoreBoard;

    @BeforeEach 
    void BeforeEach() {
        scoreBoard = new WorldCupScoreBoard();
    }

    @Test
    void addMatch_emptyScoreBoard_shouldAddAMatch() {
        scoreBoard.startGame(MATCH_1);
        assertEquals(STR_MATCH_1, scoreBoard.getSummary());
    }  

    @Test
    void addMatch_NonEmptyScoreBoard_shouldAddAMatch() {
        scoreBoard.startGame(MATCH_1);
        scoreBoard.startGame(MATCH_2);
        assertEquals(STR_MATCH_2 + "\n" + STR_MATCH_1, scoreBoard.getSummary());
    } 

    @Test
    void removeMatch_oneMatchScoreBoard_shouldBeEmpty() {
        scoreBoard.startGame(MATCH_1);
        scoreBoard.finishGame(MATCH_1);
        assertEquals("", scoreBoard.getSummary());
    }

    @Test
    void removeMatch_NonEmptyScoreBoard_shouldRemoveMatch() {
        scoreBoard.startGame(MATCH_1);
        scoreBoard.startGame(MATCH_2);
        scoreBoard.finishGame(MATCH_1);
        assertEquals(STR_MATCH_2, scoreBoard.getSummary());
    }

    @Test
    void updateScore_NonEmptyScoreBoard_shouldUpdateMatchScore() {
        FootballMatch givenMatch = new FootballMatch("TEAM E", "TEAM F", 0, 0, LocalDateTime.now());
        String expectedString = "TEAM E 0 - TEAM F 1";
        scoreBoard.startGame(MATCH_1);
        scoreBoard.startGame(givenMatch);
        scoreBoard.updateScore(givenMatch, 0, 1);
        assertEquals(expectedString + "\n" + STR_MATCH_1, scoreBoard.getSummary());
    }

    @ParameterizedTest
    @MethodSource("getSummaryTestParameters")
    void getSummary_shouldReturnValidString(List<FootballMatch> matches, String expectedSummary) {
        matches.forEach(match -> scoreBoard.startGame(match));
        assertEquals(expectedSummary, scoreBoard.getSummary());
    }

    private static Stream<Arguments> getSummaryTestParameters() {
        return Stream.of(
            Arguments.of(new ArrayList<>(),                                 ""),
            Arguments.of(Arrays.asList(MATCH_1),                            STR_MATCH_1),
            Arguments.of(Arrays.asList(MATCH_1, MATCH_2),                   STR_MATCH_2 + "\n" + STR_MATCH_1),
            Arguments.of(Arrays.asList(MATCH_3, MATCH_1),                   STR_MATCH_3 + "\n" + STR_MATCH_1),
            Arguments.of(Arrays.asList(MATCH_1, MATCH_2, MATCH_3),          STR_MATCH_2 + "\n" + STR_MATCH_1 + "\n" + STR_MATCH_3),
            Arguments.of(Arrays.asList(MATCH_3, MATCH_2, MATCH_1, MATCH_4), STR_MATCH_4 + "\n" + STR_MATCH_2 + "\n" + STR_MATCH_3 + "\n" + STR_MATCH_1)
        );
    }

}
